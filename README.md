#** mmcblk0 Extractor v1.0.1**#
##by SuperR##

##This tool in intended to extract the full disk image from your Android device (mmcblk0). Once extracted, it allows you to open the image in testdisk to extract individual partitions.##


##**USAGE:**##

In your terminal, type the following where "/home/user/extractor/" is the directory where the script lives:

```
cd /home/user/extractor/
./extract
```

**OR**

Double-click the extract file and choose "Run in Terminal" if your OS supports it.
